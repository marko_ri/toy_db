use crate::{error::DbError, Index};

use std::{
    fs::File,
    io::{Cursor, Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

// TODO: test config (std::option_env + lazy_static ?)
pub const USERNAME_SIZE: usize = 8;
pub const EMAIL_SIZE: usize = 8;
const PAGE_SIZE: usize = 64;
const TABLE_MAX_PAGES: usize = 8;
const LEAF_NODE_SPACE_FOR_CELLS: usize = PAGE_SIZE - LeafHeader::size();
pub const LEAF_NODE_MAX_CELLS: usize = 2;
// else
// pub const USERNAME_SIZE: usize = 32;
// pub const EMAIL_SIZE: usize = 255;
// const PAGE_SIZE: usize = 4096;
// const TABLE_MAX_PAGES: usize = 100;
// const LEAF_NODE_SPACE_FOR_CELLS: usize = PAGE_SIZE - LeafHeader::size();
// pub const LEAF_NODE_MAX_CELLS: usize = LEAF_NODE_SPACE_FOR_CELLS / Cell::size();
// end

const INTERNAL_NODE_SPACE_FOR_CELLS: usize = PAGE_SIZE - InternalHeader::size();
pub const INTERNAL_NODE_MAX_CELLS: usize = INTERNAL_NODE_SPACE_FOR_CELLS / InternalCell::size();
pub const INTERNAL_NODE_MAX_KEYS: usize = 3;
pub const LEAF_NODE_RIGHT_SPLIT_COUNT: usize = (LEAF_NODE_MAX_CELLS + 1) / 2;
pub const LEAF_NODE_LEFT_SPLIT_COUNT: usize =
    (LEAF_NODE_MAX_CELLS + 1) - LEAF_NODE_RIGHT_SPLIT_COUNT;
pub const INVALID_PAGE_NUM: u32 = Index::MAX;

#[derive(Default, Clone)]
#[repr(u8)]
pub enum PageType {
    Internal,
    #[default]
    Leaf,
}

impl std::convert::TryFrom<u8> for PageType {
    type Error = DbError;

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            x if x == PageType::Internal as u8 => Ok(PageType::Internal),
            x if x == PageType::Leaf as u8 => Ok(PageType::Leaf),
            _ => Err(DbError::Io("enum deserialization error".to_string())),
        }
    }
}

#[derive(Default, Clone)]
struct Header {
    page_type: PageType,
    is_root: bool,
    parent_idx: Index,
}

impl Header {
    const fn size() -> usize {
        1 + 1 + std::mem::size_of::<Index>()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        let page_type = self.page_type.clone() as u8;
        result.extend(page_type.to_ne_bytes());
        if self.is_root {
            result.push(1);
        } else {
            result.push(0);
        }
        result.extend(self.parent_idx.to_ne_bytes());
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut page_type_bytes = [0];
        reader.read_exact(&mut page_type_bytes)?;
        let page_type: PageType = u8::from_ne_bytes(page_type_bytes).try_into()?;
        let mut is_root_bytes = [0];
        reader.read_exact(&mut is_root_bytes)?;
        let is_root = match is_root_bytes {
            [0] => false,
            [1] => true,
            _ => return Err(DbError::Io("can not deserialize is_root".to_string())),
        };
        let mut parent_idx_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut parent_idx_bytes)?;
        let parent_idx = Index::from_ne_bytes(parent_idx_bytes);

        Ok(Self {
            page_type,
            is_root,
            parent_idx,
        })
    }
}

#[derive(Default, Clone)]
struct LeafHeader {
    header: Header,
    num_cells: Index,
    next_leaf: Index,
}

impl LeafHeader {
    const fn size() -> usize {
        Header::size() + 2 * std::mem::size_of::<Index>()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.header.serialize());
        result.extend(self.num_cells.to_ne_bytes());
        result.extend(self.next_leaf.to_ne_bytes());
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let header = Header::from_bytes(reader)?;
        let mut num_cells_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut num_cells_bytes)?;
        let mut next_leaf_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut next_leaf_bytes)?;
        Ok(Self {
            header,
            num_cells: Index::from_ne_bytes(num_cells_bytes),
            next_leaf: Index::from_ne_bytes(next_leaf_bytes),
        })
    }
}

#[derive(Default, Clone)]
pub struct Cell {
    pub key: Index,
    pub value: Row,
}

impl Cell {
    const fn size() -> usize {
        std::mem::size_of::<Index>() + Row::size()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.key.to_ne_bytes());
        result.extend(self.value.serialize());
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut key_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut key_bytes)?;
        let value = Row::from_bytes(reader)?;
        Ok(Self {
            key: Index::from_ne_bytes(key_bytes),
            value,
        })
    }

    pub fn set_val(&mut self, row: Row) {
        self.key = row.id;
        self.value.id = row.id;
        self.value.username = row.username;
        self.value.email = row.email;
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Row {
    pub id: Index,
    username: [u8; USERNAME_SIZE],
    email: [u8; EMAIL_SIZE],
}

impl Default for Row {
    fn default() -> Self {
        Self {
            id: 0,
            username: [0; USERNAME_SIZE],
            email: [0; EMAIL_SIZE],
        }
    }
}

impl Row {
    const fn size() -> usize {
        std::mem::size_of::<Index>() + USERNAME_SIZE + EMAIL_SIZE
    }

    pub fn new(id: Index, username: &str, email: &str) -> Self {
        assert!(id != 0);
        let u_len = username.len();
        assert!(u_len < USERNAME_SIZE);
        let e_len = email.len();
        assert!(e_len < EMAIL_SIZE);

        let mut temp_username = [0; USERNAME_SIZE];
        let mut temp_email = [0; EMAIL_SIZE];
        temp_username[..u_len].copy_from_slice(username.as_bytes());
        temp_email[..e_len].copy_from_slice(email.as_bytes());
        Self {
            id,
            username: temp_username,
            email: temp_email,
        }
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend_from_slice(&self.id.to_ne_bytes());
        result.extend_from_slice(&self.username);
        result.extend_from_slice(&self.email);
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut id_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut id_bytes)?;
        let mut username = [0; USERNAME_SIZE];
        reader.read_exact(&mut username)?;
        let mut email = [0; EMAIL_SIZE];
        reader.read_exact(&mut email)?;
        Ok(Self {
            id: Index::from_ne_bytes(id_bytes),
            username,
            email,
        })
    }
}

impl std::fmt::Display for Row {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.id == 0 {
            write!(f, "")
        } else {
            let u_len = self
                .username
                .iter()
                .position(|&x| x == 0)
                .unwrap_or(self.username.len());
            let e_len = self
                .email
                .iter()
                .position(|&x| x == 0)
                .unwrap_or(self.email.len());
            write!(
                f,
                "{}, {}, {}",
                self.id,
                String::from_utf8(self.username[0..u_len].into()).unwrap(),
                String::from_utf8(self.email[0..e_len].into()).unwrap()
            )
        }
    }
}

#[derive(Clone)]
struct LeafBody {
    cells: Vec<Cell>,
}

impl Default for LeafBody {
    fn default() -> Self {
        Self {
            cells: vec![Cell::default(); LEAF_NODE_MAX_CELLS],
        }
    }
}

impl LeafBody {
    fn serialize(&self) -> Vec<u8> {
        self.cells.iter().fold(Vec::new(), |mut acc, cell| {
            acc.extend(cell.serialize());
            acc
        })
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut cells = Vec::new();
        let num_chunks = reader.get_ref().len() / Cell::size();
        for _ in 0..num_chunks {
            cells.push(Cell::from_bytes(reader)?);
        }

        Ok(Self { cells })
    }
}

pub struct Table {
    pub root_page_idx: Index,
    pub root_page_type: PageType,
    pub pager: Pager,
}

impl Table {
    pub fn new(pager: Pager) -> Self {
        Self {
            root_page_idx: 0,
            root_page_type: PageType::Leaf,
            pager,
        }
    }

    pub fn find_slot(&mut self, key: Index) -> Result<(Index, Index), DbError> {
        let root_page_idx = self.root_page_idx;
        match self.root_page_type {
            PageType::Leaf => self.find_slot_leaf(root_page_idx, key),
            PageType::Internal => self.find_slot_internal(root_page_idx, key),
        }
    }

    fn find_slot_leaf(&mut self, page_idx: Index, key: Index) -> Result<(Index, Index), DbError> {
        let page = self.pager.fetch_leaf_page_mut(page_idx)?;
        let cell_idx = binary_search_leaf(key, page)?;
        Ok((page_idx, cell_idx))
    }

    fn find_slot_internal(
        &mut self,
        page_idx: Index,
        key: Index,
    ) -> Result<(Index, Index), DbError> {
        let page = self.pager.fetch_internal_page_mut(page_idx)?;
        let result = binary_search_internal(key, page);

        let child_idx = page.child(result);
        match self.pager.page_cached(child_idx) {
            Page::Internal(_) => self.find_slot_internal(child_idx, key),
            Page::Leaf(_) => self.find_slot_leaf(child_idx, key),
        }
    }

    pub fn max_key(&self, page: &Page) -> Result<Index, DbError> {
        match page {
            Page::Leaf(leaf_page) => Ok(leaf_page.max_key()),
            Page::Internal(internal_page) => {
                let right_child = self.pager.page_cached(internal_page.right_child());
                self.max_key(right_child)
            }
        }
    }

    pub fn set_root(&mut self, page: Page) {
        self.root_page_type = PageType::Internal;
        self.pager.pages[self.root_page_idx as usize] = Some(page);
    }
}

pub fn binary_search_leaf(key: Index, page: &LeafPage) -> Result<Index, DbError> {
    let mut max_idx = page.num_cells();
    let mut min_idx = 0;
    while max_idx != min_idx {
        let idx = (max_idx + min_idx) / 2;
        let key_at_idx = page.cell(idx).key;
        match key.cmp(&key_at_idx) {
            std::cmp::Ordering::Equal => {
                return Err(DbError::ExecuteFail("duplicate key".to_string()));
            }
            std::cmp::Ordering::Less => {
                max_idx = idx;
            }
            std::cmp::Ordering::Greater => {
                min_idx = idx + 1;
            }
        }
    }
    Ok(min_idx)
}

pub fn binary_search_internal(key: Index, page: &InternalPage) -> Index {
    let mut max_idx = page.num_keys();
    let mut min_idx = 0;
    while max_idx != min_idx {
        let idx = (max_idx + min_idx) / 2;
        let key_to_right = page.key(idx);
        if key_to_right >= key {
            max_idx = idx;
        } else {
            min_idx = idx + 1;
        }
    }
    min_idx
}

pub struct DbCursor<'a> {
    pub table: &'a mut Table,
    pub page_idx: Index,
    pub cell_idx: Index,
    pub end_of_table: bool,
}

impl<'a> DbCursor<'a> {
    pub fn table_start(table: &'a mut Table) -> Result<Self, DbError> {
        let (page_idx, cell_idx) = table.find_slot(0)?;
        let mut db_cursor = DbCursor {
            table,
            page_idx,
            cell_idx,
            end_of_table: false,
        };
        let page = db_cursor
            .table
            .pager
            .fetch_leaf_page_mut(db_cursor.page_idx)?;
        db_cursor.end_of_table = page.num_cells() == 0;
        Ok(db_cursor)
    }

    pub fn advance(&mut self) -> Result<(), DbError> {
        let page = self.table.pager.fetch_leaf_page_mut(self.page_idx)?;
        self.cell_idx += 1;
        if self.cell_idx >= page.num_cells() {
            // advance to next leaf page
            let next_page_idx = page.next_leaf();
            if next_page_idx == 0 {
                // this was rightmost leaf
                self.end_of_table = true;
            } else {
                self.page_idx = next_page_idx;
                self.cell_idx = 0;
            }
        }
        Ok(())
    }

    pub fn leaf_page_row(&mut self) -> Result<&mut Row, DbError> {
        let cell_idx = self.cell_idx;
        let page = self.table.pager.fetch_leaf_page_mut(self.page_idx)?;
        let cell = page.cell_mut(cell_idx);
        Ok(&mut cell.value)
    }
}

pub enum PagerType {
    Memory,
    Disk(PathBuf),
}

#[derive(Clone)]
pub enum Page {
    Leaf(LeafPage),
    Internal(InternalPage),
}

impl Page {
    fn serialize(&self) -> Vec<u8> {
        match self {
            Page::Leaf(leaf_page) => leaf_page.serialize(),
            Page::Internal(internal_page) => internal_page.serialize(),
        }
    }

    pub fn set_parent(&mut self, parent_idx: Index) {
        match self {
            Page::Leaf(leaf_page) => leaf_page.set_parent(parent_idx),
            Page::Internal(internal_page) => internal_page.set_parent(parent_idx),
        }
    }

    pub fn parent(&self) -> Index {
        match self {
            Page::Leaf(leaf_page) => leaf_page.parent(),
            Page::Internal(internal_page) => internal_page.parent(),
        }
    }

    pub fn set_is_root(&mut self, is_root: bool) {
        match self {
            Page::Leaf(leaf_page) => leaf_page.set_is_root(is_root),
            Page::Internal(internal_page) => internal_page.set_is_root(is_root),
        }
    }

    pub fn is_root(&self) -> bool {
        match self {
            Page::Leaf(leaf_page) => leaf_page.is_root(),
            Page::Internal(internal_page) => internal_page.is_root(),
        }
    }
}

#[derive(Clone)]
struct InternalHeader {
    header: Header,
    num_keys: Index,
    right_child_idx: Index,
}

impl Default for InternalHeader {
    fn default() -> Self {
        Self {
            header: Header {
                page_type: PageType::Internal,
                is_root: false,
                parent_idx: 0,
            },
            num_keys: 0,
            // Necessary because the root page number is 0; by not initializing an internal
            // pages's right child to an invalid page number when initializing the page, we may
            // end up with 0 as the page's right child, which makes the page a parent of the root
            right_child_idx: INVALID_PAGE_NUM,
        }
    }
}

impl InternalHeader {
    const fn size() -> usize {
        Header::size() + 2 * std::mem::size_of::<Index>()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.header.serialize());
        result.extend(self.num_keys.to_ne_bytes());
        result.extend(self.right_child_idx.to_ne_bytes());
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let header = Header::from_bytes(reader)?;
        let mut num_keys_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut num_keys_bytes)?;
        let mut right_child_idx_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut right_child_idx_bytes)?;
        Ok(Self {
            header,
            num_keys: Index::from_ne_bytes(num_keys_bytes),
            right_child_idx: Index::from_ne_bytes(right_child_idx_bytes),
        })
    }
}

#[derive(Default, Clone)]
pub struct InternalCell {
    child_idx: Index,
    // Every key should be the maximum key contained in the child to its left
    key: Index,
}

impl InternalCell {
    const fn size() -> usize {
        2 * std::mem::size_of::<Index>()
    }

    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.child_idx.to_ne_bytes());
        result.extend(self.key.to_ne_bytes());
        result
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut child_idx_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut child_idx_bytes)?;
        let mut key_bytes = [0; std::mem::size_of::<Index>()];
        reader.read_exact(&mut key_bytes)?;
        Ok(Self {
            child_idx: Index::from_ne_bytes(child_idx_bytes),
            key: Index::from_ne_bytes(key_bytes),
        })
    }
}

#[derive(Clone)]
struct InternalBody {
    cells: Vec<InternalCell>,
}

impl Default for InternalBody {
    fn default() -> Self {
        Self {
            cells: vec![InternalCell::default(); INTERNAL_NODE_MAX_CELLS],
        }
    }
}

impl InternalBody {
    fn serialize(&self) -> Vec<u8> {
        self.cells.iter().fold(Vec::new(), |mut acc, cell| {
            acc.extend(cell.serialize());
            acc
        })
    }

    fn from_bytes(reader: &mut Cursor<&[u8]>) -> Result<Self, DbError> {
        let mut cells = Vec::new();
        let num_chunks = reader.get_ref().len() / InternalCell::size();
        for _ in 0..num_chunks {
            cells.push(InternalCell::from_bytes(reader)?);
        }

        Ok(Self { cells })
    }
}

#[derive(Default, Clone)]
pub struct InternalPage {
    header: InternalHeader,
    body: InternalBody,
}

impl InternalPage {
    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.header.serialize());
        result.extend(self.body.serialize());

        // pading with 0s
        result.resize(PAGE_SIZE, 0);

        result
    }

    fn from_bytes(bytes: &[u8]) -> Result<Self, DbError> {
        let mut reader = Cursor::new(bytes);
        let header = InternalHeader::from_bytes(&mut reader)?;
        let body = InternalBody::from_bytes(&mut reader)?;
        Ok(Self { header, body })
    }

    pub fn init(&mut self) {
        self.header.header.is_root = false;
        self.header.num_keys = 0;
        self.header.right_child_idx = INVALID_PAGE_NUM;
    }

    pub fn is_full(&self) -> bool {
        self.header.num_keys as usize == INTERNAL_NODE_MAX_KEYS
    }

    // For an internal page, the maximum key is always its right key.
    pub fn max_key(&self) -> Index {
        self.body.cells[self.header.num_keys as usize - 1].key
    }

    pub fn set_num_keys(&mut self, num_keys: Index) {
        self.header.num_keys = num_keys;
    }

    pub fn num_keys(&self) -> Index {
        self.header.num_keys
    }

    pub fn set_child(&mut self, cell_idx: Index, child_page_idx: Index) {
        let num_keys = self.header.num_keys;
        match cell_idx.cmp(&num_keys) {
            std::cmp::Ordering::Greater => {
                panic!("tried to access child_num > num_keys");
            }
            std::cmp::Ordering::Equal => {
                self.header.right_child_idx = child_page_idx;
            }
            std::cmp::Ordering::Less => {
                self.body.cells[cell_idx as usize].child_idx = child_page_idx;
            }
        }
    }

    pub fn child(&self, cell_idx: Index) -> Index {
        let num_keys = self.header.num_keys;
        match cell_idx.cmp(&num_keys) {
            std::cmp::Ordering::Greater => {
                panic!("tried to access child_num > num_keys");
            }
            std::cmp::Ordering::Equal => {
                let right_child = self.right_child();
                if right_child == INVALID_PAGE_NUM {
                    panic!("tried to access right child, but was invalid page");
                }
                right_child
            }
            std::cmp::Ordering::Less => {
                let child = self.body.cells[cell_idx as usize].child_idx;
                if child == INVALID_PAGE_NUM {
                    panic!("tried to access child of page, but was invalid page");
                }
                child
            }
        }
    }

    pub fn set_val(&mut self, other: Self) {
        self.header = other.header;
        self.body = other.body;
    }

    pub fn set_key(&mut self, cell_idx: Index, val: Index) {
        self.body.cells[cell_idx as usize].key = val;
    }

    pub fn key(&self, cell_idx: Index) -> Index {
        self.body.cells[cell_idx as usize].key
    }

    pub fn set_right_child(&mut self, right_child: Index) {
        self.header.right_child_idx = right_child;
    }

    pub fn right_child(&self) -> Index {
        self.header.right_child_idx
    }

    pub fn set_is_root(&mut self, is_root: bool) {
        self.header.header.is_root = is_root;
    }

    pub fn is_root(&self) -> bool {
        self.header.header.is_root
    }

    pub fn parent(&self) -> Index {
        self.header.header.parent_idx
    }

    pub fn set_parent(&mut self, parent_idx: Index) {
        self.header.header.parent_idx = parent_idx;
    }

    pub fn update_key(&mut self, old_key: Index, new_key: Index) {
        let old_child_idx = binary_search_internal(old_key, self);
        self.set_key(old_child_idx, new_key);
    }

    pub fn cell(&self, cell_idx: Index) -> &InternalCell {
        &self.body.cells[cell_idx as usize]
    }

    pub fn set_cell(&mut self, cell_idx: Index, cell: InternalCell) {
        self.body.cells[cell_idx as usize] = cell;
    }
}

#[derive(Default, Clone)]
pub struct LeafPage {
    header: LeafHeader,
    body: LeafBody,
}

impl LeafPage {
    fn serialize(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.extend(self.header.serialize());
        result.extend(self.body.serialize());

        // pading with 0s
        result.resize(PAGE_SIZE, 0);

        result
    }

    fn from_bytes(bytes: &[u8]) -> Result<Self, DbError> {
        let mut reader = Cursor::new(bytes);
        let header = LeafHeader::from_bytes(&mut reader)?;
        let body = LeafBody::from_bytes(&mut reader)?;
        Ok(Self { header, body })
    }

    pub fn cell_mut(&mut self, cell_idx: Index) -> &mut Cell {
        &mut self.body.cells[cell_idx as usize]
    }

    pub fn cell(&self, cell_idx: Index) -> &Cell {
        &self.body.cells[cell_idx as usize]
    }

    pub fn num_cells(&self) -> Index {
        self.header.num_cells
    }

    pub fn set_num_cells(&mut self, num_cells: Index) {
        self.header.num_cells = num_cells;
    }

    pub fn is_full(&self) -> bool {
        self.num_cells() as usize == LEAF_NODE_MAX_CELLS
    }

    pub fn is_root(&self) -> bool {
        self.header.header.is_root
    }

    pub fn set_is_root(&mut self, is_root: bool) {
        self.header.header.is_root = is_root;
    }

    pub fn set_val(&mut self, other: Self) {
        self.header = other.header;
        self.body = other.body;
    }

    pub fn clear_header(&mut self) {
        self.header.header.is_root = false;
        self.header.num_cells = 0;
    }

    pub fn key(&self, cell_idx: Index) -> Index {
        self.body.cells[cell_idx as usize].key
    }

    pub fn set_next_leaf(&mut self, leaf_idx: Index) {
        self.header.next_leaf = leaf_idx;
    }

    pub fn next_leaf(&self) -> Index {
        self.header.next_leaf
    }

    pub fn set_parent(&mut self, parent_idx: Index) {
        self.header.header.parent_idx = parent_idx;
    }

    pub fn parent(&self) -> Index {
        self.header.header.parent_idx
    }

    pub fn max_key(&self) -> Index {
        // For a leaf page, it’s the key at the maximum index:
        self.key(self.num_cells() - 1)
    }
}

pub struct Pager {
    file: Option<File>,
    // TODO: clear cache strategy
    pages: Vec<Option<Page>>,
    pub num_pages: Index,
}

impl Pager {
    pub fn new(file: Option<File>) -> Result<Self, DbError> {
        let num_pages = match file.as_ref() {
            None => 0,
            Some(file) => (file.metadata().unwrap().len() as usize / PAGE_SIZE) as Index,
        };
        let mut pager = Self {
            file,
            pages: vec![None; TABLE_MAX_PAGES],
            num_pages,
        };
        if num_pages == 0 {
            let root_page = pager.fetch_leaf_page_mut(0)?;
            root_page.set_is_root(true);
        }
        Ok(pager)
    }

    pub fn fetch_leaf_page_mut(&mut self, page_idx: Index) -> Result<&mut LeafPage, DbError> {
        self.cache_page(page_idx as usize, PageType::Leaf)?;
        match self.pages[page_idx as usize].as_mut() {
            Some(Page::Leaf(page)) => Ok(page),
            _ => unreachable!(),
        }
    }

    pub fn fetch_internal_page_mut(
        &mut self,
        page_idx: Index,
    ) -> Result<&mut InternalPage, DbError> {
        self.cache_page(page_idx as usize, PageType::Internal)?;
        match self.pages[page_idx as usize].as_mut() {
            Some(Page::Internal(page)) => Ok(page),
            _ => unreachable!(),
        }
    }

    // It first looks in its cache. On a cache miss, it copies data from disk into memory
    fn cache_page(&mut self, page_idx: usize, page_type: PageType) -> Result<(), DbError> {
        if page_idx > TABLE_MAX_PAGES {
            return Err(DbError::ExecuteFail(
                "tried to fetch page number out of bounds".to_string(),
            ));
        }
        if self.pages[page_idx].is_none() {
            // Cache miss, load from file, or allocate mem
            let page = match self.file.as_mut() {
                None => {
                    self.num_pages += 1;
                    match page_type {
                        PageType::Internal => Page::Internal(InternalPage::default()),
                        PageType::Leaf => Page::Leaf(LeafPage::default()),
                    }
                }
                Some(file) => {
                    if page_idx < self.num_pages as usize {
                        file.seek(SeekFrom::Start((page_idx * PAGE_SIZE) as u64))?;
                        let mut buf = vec![0; PAGE_SIZE];
                        file.read_exact(&mut buf)?;
                        match page_type {
                            PageType::Internal => Page::Internal(InternalPage::from_bytes(&buf)?),
                            PageType::Leaf => Page::Leaf(LeafPage::from_bytes(&buf)?),
                        }
                    } else {
                        // If the requested page lies outside the bounds of the file,
                        // we know it should be blank, so we just allocate some memory
                        self.num_pages += 1;
                        match page_type {
                            PageType::Internal => Page::Internal(InternalPage::default()),
                            PageType::Leaf => Page::Leaf(LeafPage::default()),
                        }
                    }
                }
            };

            self.pages[page_idx] = Some(page);
        }

        Ok(())
    }

    pub fn flush_page(&mut self, page_idx: Index) -> Result<(), DbError> {
        match self.file.as_mut() {
            None => Ok(()),
            Some(file) => {
                let Some(page) = self.pages[page_idx as usize].as_ref() else {
                    return Ok(());
                };
                let buf = page.serialize();
                file.seek(SeekFrom::Start((page_idx as usize * PAGE_SIZE) as u64))?;
                file.write_all(&buf)?;
                file.flush()?;
                Ok(())
            }
        }
    }

    // Until we start recycling free pages, new pages will always
    // go onto the end of the database file
    pub fn unused_page(&self) -> Index {
        self.num_pages
    }

    pub fn page_cached(&self, page_idx: Index) -> &Page {
        self.pages[page_idx as usize]
            .as_ref()
            .expect("page is not in the cache")
    }

    pub fn page_cached_mut(&mut self, page_idx: Index) -> &mut Page {
        self.pages[page_idx as usize]
            .as_mut()
            .expect("page is not in the cache")
    }

    pub fn take_page_cached(&mut self, page_idx: Index) -> Page {
        self.pages[page_idx as usize]
            .take()
            .expect("page is not in the cache")
    }

    pub fn page_leaf_cached(&self, page_idx: Index) -> &LeafPage {
        match self.page_cached(page_idx) {
            Page::Leaf(page_leaf) => page_leaf,
            _ => panic!("cached page is not 'leaf page'"),
        }
    }

    pub fn page_internal_cached(&self, page_idx: Index) -> &InternalPage {
        match self.page_cached(page_idx) {
            Page::Internal(page_internal) => page_internal,
            _ => panic!("cached page is not 'internal page'"),
        }
    }

    pub fn set_page(&mut self, page_idx: Index, page: Page) {
        self.pages[page_idx as usize] = Some(page);
    }
}
