use crate::{
    btree::{Row, EMAIL_SIZE, USERNAME_SIZE},
    error::DbError,
    Statement, StatementType,
};

pub fn prepare_statement(command: &str) -> Result<Statement, DbError> {
    let splits: Vec<&str> = command.split_whitespace().collect();
    match splits[0] {
        "insert" => prepare_insert(&splits),
        "select" => Ok(Statement {
            stmt_type: StatementType::Select,
            row_to_insert: None,
        }),
        x => Err(DbError::UnrecognizedStatement(x.to_string())),
    }
}

fn prepare_insert(command: &[&str]) -> Result<Statement, DbError> {
    if command.len() < 4 {
        return Err(DbError::SyntaxError(
            "wrong number of arguments".to_string(),
        ));
    }

    let id = match command[1].trim().parse::<u32>() {
        Ok(n) if n > 0 => n,
        _ => {
            return Err(DbError::SyntaxError(
                "first arg should be int greater than 0".to_string(),
            ))
        }
    };

    let username = command[2].trim();
    if username.len() > USERNAME_SIZE {
        return Err(DbError::SyntaxError("username too long".to_string()));
    }

    let email = command[3].trim();
    if email.len() > EMAIL_SIZE {
        return Err(DbError::SyntaxError("email too long".to_string()));
    }

    Ok(Statement {
        stmt_type: StatementType::Insert,
        row_to_insert: Some(Row::new(id, username, email)),
    })
}
