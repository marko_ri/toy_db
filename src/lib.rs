use std::{
    fs::OpenOptions,
    io::{self, Write},
};

mod btree;
use btree::Pager;
pub use btree::{PagerType, Row, Table};
mod command_processor;
use command_processor::prepare_statement;
mod error;
pub use error::DbError;
mod virtual_machine;
use virtual_machine::do_meta_command;
pub use virtual_machine::execute_statement;

pub type Index = u32;

#[derive(Debug)]
pub enum StatementType {
    Insert,
    Select,
}

#[derive(Debug)]
pub struct Statement {
    pub stmt_type: StatementType,
    pub row_to_insert: Option<Row>,
}

pub struct ProgramParams<R, W> {
    pub reader: R,
    pub writer: W,
    pub pager_type: PagerType,
}

pub fn run<R, W>(mut params: ProgramParams<R, W>) -> Result<String, DbError>
where
    R: io::BufRead,
    W: Write,
{
    let mut result = String::new();
    let mut table = db_open(params.pager_type)?;

    loop {
        let command = read_input(&mut params.reader, &mut params.writer);
        if command.is_empty() {
            continue;
        }

        if command.starts_with('.') {
            match do_meta_command(&command, &mut table) {
                Ok(should_continue) => {
                    if should_continue {
                        continue;
                    } else {
                        return Ok(result);
                    }
                }
                Err(e) => {
                    eprintln!("{e}");
                    continue;
                }
            }
        }

        match prepare_statement(&command).and_then(|stmt| execute_statement(stmt, &mut table)) {
            Ok(exe_result) => {
                result.push_str(&exe_result);
                result.push('\n');
            }
            Err(e) => {
                result = e.to_string();
                eprintln!("{e}");
                continue;
            }
        }
    }
}

fn read_input<R, W>(mut reader: R, mut writer: W) -> String
where
    R: io::BufRead,
    W: Write,
{
    write!(&mut writer, "db> ").expect("unable to write");
    writer.flush().expect("unable to flush");

    let mut input_buffer = String::new();
    reader.read_line(&mut input_buffer).expect("unable to read");
    String::from(input_buffer.trim())
}

pub fn db_open(pager_type: PagerType) -> Result<Table, DbError> {
    let pager = match pager_type {
        PagerType::Memory => Pager::new(None)?,
        PagerType::Disk(path) => {
            let file = OpenOptions::new()
                .write(true)
                .create(true)
                .read(true)
                .open(path)?;
            Pager::new(Some(file))?
        }
    };
    Ok(Table::new(pager))
}

pub fn db_close(table: &mut Table) -> Result<(), DbError> {
    for i in 0..table.pager.num_pages {
        table.pager.flush_page(i)?;
    }

    Ok(())
}
