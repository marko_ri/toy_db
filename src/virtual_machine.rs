use crate::{btree::*, db_close, error::DbError, Index, Statement, StatementType};

pub fn execute_statement(stmt: Statement, table: &mut Table) -> Result<String, DbError> {
    match stmt.stmt_type {
        StatementType::Insert => execute_insert(stmt, table),
        StatementType::Select => execute_select(stmt, table),
    }
}

fn execute_insert(stmt: Statement, table: &mut Table) -> Result<String, DbError> {
    match stmt.row_to_insert {
        Some(row_to_insert) => {
            let (page_idx, cell_idx) = table.find_slot(row_to_insert.id)?;
            let mut db_cursor = DbCursor {
                table,
                page_idx,
                cell_idx,
                end_of_table: false,
            };
            insert_page_leaf(&mut db_cursor, row_to_insert)?;
            Ok(format!("Executed: {:?}", stmt.stmt_type))
        }
        None => Err(DbError::ExecuteFail("nothing to insert".to_string())),
    }
}

fn insert_page_leaf(db_cursor: &mut DbCursor, value: Row) -> Result<(), DbError> {
    let page = db_cursor
        .table
        .pager
        .fetch_leaf_page_mut(db_cursor.page_idx)?;
    if page.is_full() {
        return split_and_insert_page_leaf(db_cursor, value);
    }
    let cell_idx = db_cursor.cell_idx;
    let num_cells = page.num_cells();
    if cell_idx < num_cells {
        // shift cells one space to the right to make room for the new cell
        for i in (cell_idx + 1..=num_cells).rev() {
            let prev_cell = page.cell(i - 1);
            let prev_row = prev_cell.value.clone();
            page.cell_mut(i).set_val(prev_row);
        }
    }
    page.set_num_cells(num_cells + 1);
    page.cell_mut(cell_idx).set_val(value);
    Ok(())
}

fn split_and_insert_page_leaf(db_cursor: &mut DbCursor, value: Row) -> Result<(), DbError> {
    let spot_idx = db_cursor.cell_idx;
    let old_page_clone = db_cursor
        .table
        .pager
        .page_leaf_cached(db_cursor.page_idx)
        .clone();
    let new_page_idx = db_cursor.table.pager.unused_page();

    {
        let new_page = db_cursor.table.pager.fetch_leaf_page_mut(new_page_idx)?;
        new_page.clear_header();
        new_page.set_parent(old_page_clone.parent());
        new_page.set_next_leaf(old_page_clone.next_leaf());
        for i in (LEAF_NODE_LEFT_SPLIT_COUNT..LEAF_NODE_MAX_CELLS + 1).rev() {
            let i = i as Index;
            let cell_idx = i % LEAF_NODE_LEFT_SPLIT_COUNT as Index;
            let dst_cell = new_page.cell_mut(cell_idx);
            match i.cmp(&(spot_idx)) {
                std::cmp::Ordering::Equal => {
                    dst_cell.set_val(value.clone());
                }
                std::cmp::Ordering::Greater => {
                    let prev_row = old_page_clone.cell(i - 1).value.clone();
                    dst_cell.set_val(prev_row);
                }
                std::cmp::Ordering::Less => {
                    let prev_row = old_page_clone.cell(i).value.clone();
                    dst_cell.set_val(prev_row);
                }
            }
        }
        new_page.set_num_cells(LEAF_NODE_RIGHT_SPLIT_COUNT as Index);
    }

    {
        let old_page_mut = db_cursor
            .table
            .pager
            .fetch_leaf_page_mut(db_cursor.page_idx)?;
        for i in (0..LEAF_NODE_LEFT_SPLIT_COUNT).rev() {
            let i = i as Index;
            let cell_idx = i % LEAF_NODE_LEFT_SPLIT_COUNT as Index;
            let dst_cell = old_page_mut.cell_mut(cell_idx);
            match i.cmp(&(spot_idx)) {
                std::cmp::Ordering::Equal => {
                    dst_cell.set_val(value.clone());
                }
                std::cmp::Ordering::Greater => {
                    let prev_row = old_page_clone.cell(i - 1).value.clone();
                    dst_cell.set_val(prev_row);
                }
                std::cmp::Ordering::Less => {
                    let prev_row = old_page_clone.cell(i).value.clone();
                    dst_cell.set_val(prev_row);
                }
            }
        }
        old_page_mut.set_next_leaf(new_page_idx);
        old_page_mut.set_num_cells(LEAF_NODE_LEFT_SPLIT_COUNT as Index);
    };

    if old_page_clone.is_root() {
        create_new_root(db_cursor.table, new_page_idx)
    } else {
        let old_page = db_cursor.table.pager.page_cached(db_cursor.page_idx);
        let parent_idx = old_page.parent();
        let new_max = db_cursor.table.max_key(old_page)?;
        let old_max = db_cursor.table.max_key(&Page::Leaf(old_page_clone))?;
        let parent = db_cursor.table.pager.fetch_internal_page_mut(parent_idx)?;
        parent.update_key(old_max, new_max);
        insert_page_internal(db_cursor.table, parent_idx, new_page_idx)
    }
}

fn insert_page_internal(
    table: &mut Table,
    parent_idx: Index,
    child_idx: Index,
) -> Result<(), DbError> {
    let child_max_key = {
        let child = table.pager.page_cached(child_idx);
        table.max_key(child)?
    };

    let right_child_idx;
    let child_max_key_idx;
    let original_num_keys;
    {
        let parent = table.pager.fetch_internal_page_mut(parent_idx)?;
        child_max_key_idx = binary_search_internal(child_max_key, parent);
        original_num_keys = parent.num_keys();
        if original_num_keys as usize >= INTERNAL_NODE_MAX_KEYS {
            return split_and_insert_page_internal(table, parent_idx, child_idx);
        }
        right_child_idx = parent.right_child();
        if right_child_idx == INVALID_PAGE_NUM {
            // An internal page with a right child of INVALID_PAGE_NUM is empty
            parent.set_right_child(child_idx);
            return Ok(());
        }

        parent.set_num_keys(original_num_keys + 1);
    }

    let right_child_max_key = {
        let right_child = table.pager.page_cached(right_child_idx);
        table.max_key(right_child)?
    };

    if child_max_key > right_child_max_key {
        // Replace right child
        let parent = table.pager.fetch_internal_page_mut(parent_idx)?;
        parent.set_child(original_num_keys, right_child_idx);
        parent.set_key(original_num_keys, right_child_max_key);
        parent.set_right_child(child_idx);
    } else {
        // Make room for the new cell
        let parent = table.pager.fetch_internal_page_mut(parent_idx)?;
        for i in (child_max_key_idx + 1..=original_num_keys).rev() {
            parent.set_cell(i, parent.cell(i - 1).clone());
        }
        parent.set_child(child_max_key_idx, child_idx);
        parent.set_key(child_max_key_idx, child_max_key);
    }

    Ok(())
}

fn split_and_insert_page_internal(
    table: &mut Table,
    parent_idx: Index,
    child_idx: Index,
) -> Result<(), DbError> {
    let mut old_page_idx = parent_idx;
    let old_max;
    let child_max;
    let new_page_idx;
    let splitting_root;
    {
        let old_page = table.pager.page_cached(old_page_idx);
        splitting_root = old_page.is_root();
        old_max = table.max_key(old_page)?;
        let child = table.pager.page_cached(child_idx);
        child_max = table.max_key(child)?;
        new_page_idx = table.pager.unused_page();
    }

    if splitting_root {
        create_new_root(table, new_page_idx)?;
        let parent = table.pager.fetch_internal_page_mut(table.root_page_idx)?;
        old_page_idx = parent.child(0);
    }

    let mut cur_page_idx;
    {
        let old_page = table.pager.fetch_internal_page_mut(old_page_idx)?;
        cur_page_idx = old_page.right_child();
        old_page.set_right_child(INVALID_PAGE_NUM);
        insert_page_internal(table, new_page_idx, cur_page_idx)?;
        let cur_page = table.pager.page_cached_mut(cur_page_idx);
        cur_page.set_parent(new_page_idx);
    }
    {
        let old_page = table.pager.page_internal_cached(old_page_idx);
        let mut old_page_children = std::collections::HashMap::new();
        for i in (INTERNAL_NODE_MAX_KEYS / 2 + 1..INTERNAL_NODE_MAX_KEYS).rev() {
            old_page_children.insert(i, old_page.child(i as Index));
        }
        // For each key until you get to the middle key, move the key
        // and the child to the new page
        for i in (INTERNAL_NODE_MAX_KEYS / 2 + 1..INTERNAL_NODE_MAX_KEYS).rev() {
            cur_page_idx = old_page_children[&i];
            insert_page_internal(table, new_page_idx, cur_page_idx)?;
            let cur_page = table.pager.page_cached_mut(cur_page_idx);
            cur_page.set_parent(new_page_idx);
            {
                let old_page = table.pager.fetch_internal_page_mut(old_page_idx)?;
                old_page.set_num_keys(old_page.num_keys() - 1);
            }
        }
    }

    {
        // Set child before middle key, which is now the highest key,
        // to be page's right child, and decrement number of keys
        let old_page = table.pager.fetch_internal_page_mut(old_page_idx)?;
        let old_num_keys = old_page.num_keys();
        let rc = old_page.child(old_num_keys - 1);
        old_page.set_right_child(rc);
        old_page.set_num_keys(old_num_keys - 1);
    }

    let destination_page_idx = {
        // determine which of the two pages after the split should
        // contain the child to be inserted,  and insert the child
        let old_page = table.pager.page_cached(old_page_idx);
        let max_after_split = table.max_key(old_page)?;
        if child_max < max_after_split {
            old_page_idx
        } else {
            new_page_idx
        }
    };

    {
        insert_page_internal(table, destination_page_idx, child_idx)?;
        let child = table.pager.page_cached_mut(child_idx);
        child.set_parent(destination_page_idx);
    }

    if splitting_root {
        let old_page = table.pager.page_cached(old_page_idx);
        let max_op = table.max_key(old_page)?;
        let parent = table.pager.fetch_internal_page_mut(table.root_page_idx)?;
        parent.update_key(old_max, max_op);
    } else {
        let old_page = table.pager.page_cached(old_page_idx);
        let max_op = table.max_key(old_page)?;
        let parent = table.pager.fetch_internal_page_mut(old_page.parent())?;
        parent.update_key(old_max, max_op);
    };

    if !splitting_root {
        let old_page = table.pager.page_cached(old_page_idx);
        let parent = old_page.parent();
        insert_page_internal(table, parent, new_page_idx)?;
        let new_page = table.pager.fetch_internal_page_mut(new_page_idx)?;
        new_page.init();
        new_page.set_parent(parent);
    }

    Ok(())
}

fn create_new_root(table: &mut Table, right_child_page_idx: Index) -> Result<(), DbError> {
    {
        // this has to be on top, so that we increment pager.num_pages
        match table.pager.page_cached(table.root_page_idx) {
            Page::Leaf(_) => {
                let rc = table.pager.fetch_leaf_page_mut(right_child_page_idx)?;
                rc.set_parent(table.root_page_idx);
            }
            Page::Internal(_) => {
                let rc = table.pager.fetch_internal_page_mut(right_child_page_idx)?;
                rc.set_parent(table.root_page_idx);
            }
        }
    }

    let left_child_page_idx = table.pager.unused_page();
    {
        // The old root is copied to the left child
        let root = table.pager.take_page_cached(table.root_page_idx);
        match root {
            Page::Leaf(leaf_root) => {
                let left_child = table.pager.fetch_leaf_page_mut(left_child_page_idx)?;
                left_child.set_val(leaf_root);
                left_child.set_is_root(false);
                left_child.set_parent(table.root_page_idx);
            }
            Page::Internal(internal_root) => {
                let left_child = table.pager.fetch_internal_page_mut(left_child_page_idx)?;
                //left_child.init();
                left_child.set_val(internal_root);
                left_child.set_is_root(false);
                left_child.set_parent(table.root_page_idx);
            }
        };
    }

    if let Page::Internal(lc) = table.pager.page_cached(left_child_page_idx) {
        let lc_right_child = lc.right_child();
        let num_keys = lc.num_keys();
        let mut lc_children = Vec::new();
        for i in 0..num_keys {
            lc_children.push(lc.child(i));
        }
        for i in 0..num_keys {
            let child_idx = lc_children[i as usize];
            match table.pager.page_cached(child_idx) {
                Page::Leaf(_) => {
                    let child = table.pager.fetch_leaf_page_mut(child_idx)?;
                    child.set_parent(left_child_page_idx);
                }
                Page::Internal(_) => {
                    let child = table.pager.fetch_internal_page_mut(child_idx)?;
                    child.set_parent(left_child_page_idx);
                }
            }
        }
        match table.pager.page_cached(lc_right_child) {
            Page::Leaf(_) => {
                let child = table.pager.fetch_leaf_page_mut(lc_right_child)?;
                child.set_parent(left_child_page_idx);
            }
            Page::Internal(_) => {
                let child = table.pager.fetch_internal_page_mut(lc_right_child)?;
                child.set_parent(left_child_page_idx);
            }
        }
    }

    {
        let left_child_max_key = {
            let lc = table.pager.page_cached(left_child_page_idx);
            table.max_key(lc)?
        };
        // Root page is a new internal page with one key and two children
        let mut root_internal = InternalPage::default();
        root_internal.set_is_root(true);
        root_internal.set_num_keys(1);
        root_internal.set_child(0, left_child_page_idx);
        root_internal.set_key(0, left_child_max_key);
        root_internal.set_right_child(right_child_page_idx);
        table.set_root(Page::Internal(root_internal));
    }
    Ok(())
}

fn execute_select(_stmt: Statement, table: &mut Table) -> Result<String, DbError> {
    let mut result = String::new();
    let mut db_cursor = DbCursor::table_start(table)?;
    // probably better as iterator, but maybe later it would need to chage
    // (e.g. go backward)
    while !db_cursor.end_of_table {
        let row = db_cursor.leaf_page_row()?;
        let temp = row.to_string();
        if !temp.is_empty() {
            result.push_str(&temp);
            result.push('\n');
            eprintln!("{temp}");
        }
        db_cursor.advance()?;
    }
    Ok(result)
}

pub fn do_meta_command(command: &str, table: &mut Table) -> Result<bool, DbError> {
    match command {
        ".exit" => {
            if let Err(e) = db_close(table) {
                eprintln!("Error while exiting: {e}");
            }
            Ok(false)
        }
        ".btree" => {
            print_tree(table, 0, 0);
            Ok(true)
        }
        _ => Err(DbError::UnrecognizedCommand(command.to_string())),
    }
}

// print only pages loaded to memory
fn print_tree(table: &Table, page_idx: Index, indentation_level: Index) {
    fn indent(level: Index) {
        for _ in 0..level {
            print!(" ");
        }
    }

    match table.pager.page_cached(page_idx) {
        Page::Leaf(page_leaf) => {
            let num_keys = page_leaf.num_cells();
            indent(indentation_level);
            println!("- leaf (size {})", num_keys);
            for i in 0..num_keys {
                indent(indentation_level + 1);
                println!("{} - parent: {}", page_leaf.key(i), page_leaf.parent());
            }
        }
        Page::Internal(page_internal) => {
            let num_keys = page_internal.num_keys();
            indent(indentation_level);
            println!("- internal (size {})", num_keys);
            for i in 0..num_keys {
                let child = page_internal.child(i);
                print_tree(table, child, indentation_level + 1);
                indent(indentation_level + 1);
                println!("- key {}", page_internal.key(i));
            }
            let child = page_internal.right_child();
            print_tree(table, child, indentation_level + 1);
        }
    }
}
