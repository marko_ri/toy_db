use std::fmt;

#[derive(Debug, PartialEq)]
pub enum DbError {
    UnrecognizedCommand(String),
    UnrecognizedStatement(String),
    SyntaxError(String),
    ExecuteFail(String),
    Full(String),
    Io(String),
    Empty(String),
}

impl std::error::Error for DbError {}

impl fmt::Display for DbError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<std::io::Error> for DbError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err.to_string())
    }
}
