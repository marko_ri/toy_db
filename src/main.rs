use std::path::PathBuf;

use toy_db::{run, DbError, PagerType, ProgramParams};

fn main() -> Result<(), DbError> {
    let stdio = std::io::stdin();
    let reader = stdio.lock();
    let writer = std::io::stdout();
    let pager_type = if let Some(table_path) = &std::env::args().nth(1) {
        PagerType::Disk(PathBuf::from(table_path))
    } else {
        PagerType::Memory
    };
    let params = ProgramParams {
        reader,
        writer,
        pager_type,
    };
    run(params)?;
    Ok(())
}
