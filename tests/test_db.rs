// TODO: more testing
use toy_db::*;

#[test]
fn test_smallest_example() {
    // TODO: change to test config
    let input = [
        "insert 1 c c",
        "insert 5 a a",
        "insert 10 d d",
        "insert 12 f f",
        "insert 14 x x",
        "insert 20 z z",
        "insert 35 h h",
        "insert 11 r r",
        ".btree",
        ".exit",
    ]
    .join("\n");
    let params = ProgramParams {
        reader: input.as_bytes(),
        writer: Vec::new(),
        pager_type: PagerType::Memory,
        //pager_type: PagerType::Disk(std::path::PathBuf::from("temp.db")),
    };
    let _actual = run(params).unwrap();
    //println!("{actual:?}");
}

#[test]
fn test_seven_leaf_tree() {
    // TODO: change to non-test config
    let input = [
        "insert 58 user58 person58@example.com",
        "insert 56 user56 person56@example.com",
        "insert 8 user8 person8@example.com",
        "insert 54 user54 person54@example.com",
        "insert 77 user77 person77@example.com",
        "insert 7 user7 person7@example.com",
        "insert 25 user25 person25@example.com",
        "insert 71 user71 person71@example.com",
        "insert 13 user13 person13@example.com",
        "insert 22 user22 person22@example.com",
        "insert 53 user53 person53@example.com",
        "insert 51 user51 person51@example.com",
        "insert 59 user59 person59@example.com",
        "insert 32 user32 person32@example.com",
        "insert 36 user36 person36@example.com",
        "insert 79 user79 person79@example.com",
        "insert 10 user10 person10@example.com",
        "insert 33 user33 person33@example.com",
        "insert 20 user20 person20@example.com",
        "insert 4 user4 person4@example.com",
        "insert 35 user35 person35@example.com",
        "insert 76 user76 person76@example.com",
        "insert 49 user49 person49@example.com",
        "insert 24 user24 person24@example.com",
        "insert 70 user70 person70@example.com",
        "insert 48 user48 person48@example.com",
        "insert 39 user39 person39@example.com",
        "insert 15 user15 person15@example.com",
        "insert 47 user47 person47@example.com",
        "insert 30 user30 person30@example.com",
        "insert 86 user86 person86@example.com",
        "insert 31 user31 person31@example.com",
        "insert 68 user68 person68@example.com",
        "insert 37 user37 person37@example.com",
        "insert 66 user66 person66@example.com",
        "insert 63 user63 person63@example.com",
        "insert 40 user40 person40@example.com",
        "insert 78 user78 person78@example.com",
        "insert 19 user19 person19@example.com",
        "insert 46 user46 person46@example.com",
        "insert 14 user14 person14@example.com",
        "insert 81 user81 person81@example.com",
        "insert 72 user72 person72@example.com",
        "insert 6 user6 person6@example.com",
        "insert 50 user50 person50@example.com",
        "insert 85 user85 person85@example.com",
        "insert 67 user67 person67@example.com",
        "insert 2 user2 person2@example.com",
        "insert 55 user55 person55@example.com",
        "insert 69 user69 person69@example.com",
        "insert 5 user5 person5@example.com",
        "insert 65 user65 person65@example.com",
        "insert 52 user52 person52@example.com",
        "insert 1 user1 person1@example.com",
        "insert 29 user29 person29@example.com",
        "insert 9 user9 person9@example.com",
        "insert 43 user43 person43@example.com",
        "insert 75 user75 person75@example.com",
        "insert 21 user21 person21@example.com",
        "insert 82 user82 person82@example.com",
        "insert 12 user12 person12@example.com",
        "insert 18 user18 person18@example.com",
        "insert 60 user60 person60@example.com",
        "insert 44 user44 person44@example.com",
        ".btree",
        ".exit",
    ]
    .join("\n");
    let params = ProgramParams {
        reader: input.as_bytes(),
        writer: Vec::new(),
        pager_type: PagerType::Memory,
    };
    run(params).unwrap();
}

/*
#[test]
fn test_print_tree() {
    let mut input = String::new();
    for i in 1..30 {
        input.push_str(&format!("insert {i} user_{i} email_{i}\n"));
    }
    // input.push_str("select\n");
    input.push_str(".btree\n");
    input.push_str(".exit\n");
    let mut output = Vec::new();
    let actual = run(input.as_bytes(), &mut output, PagerType::Memory).unwrap();
    println!("{actual:?}");
}

#[test]
fn memdb_insert_one_row() {
    let input = ["insert 1 user1 user1@email.com", "select", ".exit"].join("\n");
    let mut output = Vec::new();
    let actual = run(input.as_bytes(), &mut output, PagerType::Memory).unwrap();
    // let output = String::from_utf8(output).expect("Not UTF-8");
    let expected = ["Executed: Insert", "1, user1, user1@email.com"].join("\n");
    assert_eq!(expected, actual.trim_end());
}

#[test]
fn memdb_insert_two_pages() {
    let mut table = Table::new(Pager::new(None)).unwrap();
    let num_rows = ROWS_PER_PAGE + 1;
    for i in 0..num_rows {
        let id = i as u32 + 1;
        let row = Row::new(id, &format!("user_{id}"), &format!("email_{id}"));
        let stmt = Statement {
            stmt_type: StatementType::Insert,
            row_to_insert: Some(row),
        };
        execute_statement(stmt, &mut table).unwrap();
    }

    assert_eq!(num_rows, table.num_rows);
    let num_pages = table.num_rows / ROWS_PER_PAGE + 1;
    assert_eq!(2, num_pages);
}

#[test]
fn memdb_insert_long_username_should_fail() {
    let username = "a".repeat(USERNAME_SIZE + 1);
    let input = format!("insert 1 {} bob@email.com\n.exit", username);
    let mut output = Vec::new();
    let actual = run(input.as_bytes(), &mut output, PagerType::Memory).unwrap();
    assert_eq!("SyntaxError(\"username too long\")", actual);
}

#[test]
fn diskdb_insert_page() {
    let file_name = "diskdb_insert_page.db";
    let mut table = db_open(PagerType::Disk(path::PathBuf::from(file_name))).unwrap();

    let row = Row::new(1, "user_1", "email_1");
    let stmt = Statement {
        stmt_type: StatementType::Insert,
        row_to_insert: Some(row),
    };
    execute_statement(stmt, &mut table).unwrap();
    assert_eq!(1, table.num_rows);

    let row = Row::new(2, "user_2", "email_2");
    let stmt = Statement {
        stmt_type: StatementType::Insert,
        row_to_insert: Some(row),
    };
    execute_statement(stmt, &mut table).unwrap();
    assert_eq!(2, table.num_rows);

    db_close(&mut table).unwrap();

    let file = fs::File::open(file_name).unwrap();
    assert_eq!(PAGE_SIZE, file.metadata().unwrap().len() as usize);
    fs::remove_file(file_name).unwrap();
}

#[test]
fn diskdb_insert_one_row_in_existing_file() {
    let file_name = "diskdb_insert_one_row_in_existing_file.db";

    let mut table = db_open(PagerType::Disk(path::PathBuf::from(file_name))).unwrap();
    let row = Row::new(1, "user_1", "email_1");
    let stmt = Statement {
        stmt_type: StatementType::Insert,
        row_to_insert: Some(row),
    };
    execute_statement(stmt, &mut table).unwrap();
    assert_eq!(1, table.num_rows);
    db_close(&mut table).unwrap();

    let mut table = db_open(PagerType::Disk(path::PathBuf::from(file_name))).unwrap();
    let row = Row::new(2, "user_2", "email_2");
    let stmt = Statement {
        stmt_type: StatementType::Insert,
        row_to_insert: Some(row),
    };
    execute_statement(stmt, &mut table).unwrap();
    assert_eq!(2, table.num_rows);

    let file = fs::File::open(file_name).unwrap();
    assert_eq!(PAGE_SIZE, file.metadata().unwrap().len() as usize);
    fs::remove_file(file_name).unwrap();
}
*/
